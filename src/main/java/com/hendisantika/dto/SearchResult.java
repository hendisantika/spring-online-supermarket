package com.hendisantika.dto;

import lombok.Builder;
import lombok.Data;
import lombok.With;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/29/22
 * Time: 22:02
 * To change this template use File | Settings | File Templates.
 */
@Data
@Builder
public class SearchResult<T> {
    @With
    private List<T> itemList;
    private Integer nextPageNum;
    private Integer nextPageSize;
    private Integer totalPageNum;
    private Boolean hasNextPage;
}
