package com.hendisantika.controller;

import com.hendisantika.dto.SearchResult;
import com.hendisantika.model.Product;
import com.hendisantika.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/29/22
 * Time: 22:10
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductRestController {

    static private final String DEFAULT_PAGE_NUM = "0";
    static private final String DEFAULT_PAGE_SIZE = "5";

    private final ProductService productService;

    @GetMapping(path = {"/by-category"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<SearchResult<Product>> searchByCategory(@RequestParam(required = true) String category,
                                                        @RequestParam(defaultValue = DEFAULT_PAGE_NUM) Integer pageNum,
                                                        @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize) {
        return productService.getProductsByCategory(category, pageNum, pageSize);
    }

    @GetMapping(path = {"/by-keyword"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<SearchResult<Product>> searchByKeyword(@RequestParam(required = true) String keyword,
                                                       @RequestParam(defaultValue = DEFAULT_PAGE_NUM) Integer pageNum,
                                                       @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize) {
        return productService.getProductsByQuerystring(keyword, pageNum, pageSize);
    }

    @GetMapping(path = {"/{productId}/relevant-products"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<SearchResult<Product>> searchRelevantProducts(@PathVariable(required = true) String productId,
                                                              @RequestParam(defaultValue = DEFAULT_PAGE_NUM) Integer pageNum,
                                                              @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize) {
        return productService.getRelevantProducts(productId, pageNum, pageSize);
    }
}
