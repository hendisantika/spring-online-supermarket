package com.hendisantika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOnlineSupermarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringOnlineSupermarketApplication.class, args);
    }

}
