package com.hendisantika.service;

import com.hendisantika.dto.SearchResult;
import com.hendisantika.model.Product;
import com.hendisantika.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/29/22
 * Time: 22:08
 * To change this template use File | Settings | File Templates.
 */
@Service
@RequiredArgsConstructor
public class ProductService {

    static private final Integer DEFAULT_PAGE_SIZE = 50;

    private final ProductRepository productRepository;

    public Mono<Product> getProductById(String productId) {
        if (isNull(productId) || productId.isBlank()) return Mono.empty();
        return productRepository.findById(productId);
    }

    public Mono<SearchResult<Product>> getProductsByQuerystring(String queryString, Integer pageNum, Integer pageSize) {
        if (isNull(queryString) || queryString.isBlank()) return Mono.empty();
        return productRepository.findByQueryString(queryString, pageNum, pageSize);
    }

    public Mono<SearchResult<Product>> getProductsByCategory(String category, Integer pageNum, Integer pageSize) {
        if (isNull(category) || category.isBlank()) return Mono.empty();
        return productRepository.findByCategory(category, pageNum, pageSize);
    }

    public Mono<SearchResult<Product>> getRelevantProducts(String productId, Integer pageNum, Integer pageSize) {
        if (isNull(productId) || productId.isBlank()) return Mono.empty();
        return productRepository.findById(productId)
                .flatMap(product ->
                        productRepository.findRelevantProducts(product.getName(), product.getCategory(), pageNum, pageSize))
                .map(searchResult ->
                        searchResult.withItemList(
                                searchResult.getItemList().stream()
                                        .filter(item -> !item.getId().equals(productId))
                                        .collect(toList())
                        ));
    }
}
