package com.hendisantika;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/30/22
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
        initializers = {ComponentTestContextInitializer.class},
        classes = {SpringOnlineSupermarketApplication.class, ComponentTestContextConfig.class}
)
@ActiveProfiles(profiles = {"component-test"})
public abstract class AbstractComponentTest {
}
