package com.hendisantika;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/30/22
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */
@TestConfiguration
@ComponentScan(basePackages = {"com.hendisantika"})
public class ComponentTestContextConfig {
}
