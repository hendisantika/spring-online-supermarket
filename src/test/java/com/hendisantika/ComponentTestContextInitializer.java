package com.hendisantika;

import com.hendisantika.component.setup.TestContainersSetup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import static com.hendisantika.component.setup.TestContainersSetup.getElasticsearchIPAddress;
import static com.hendisantika.component.setup.TestContainersSetup.getElasticsearchPort;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/30/22
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
public class ComponentTestContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

        // start up and initialize test containers
        TestContainersSetup.initTestContainers(configurableApplicationContext.getEnvironment());

        // alter spring boot system properties so that it will connect to test containers and wiremock
        TestPropertyValues values = TestPropertyValues.of(
                "spring.elasticsearch.uris=http://" + getElasticsearchIPAddress() + ":" + getElasticsearchPort(),
                "spring.data.elasticsearch.client.reactive.endpoints=" + getElasticsearchIPAddress() + ":" + getElasticsearchPort()
        );

        values.applyTo(configurableApplicationContext);

        log.info("======= Customized properties settings =======");
        log.info("spring.elasticsearch.uris=http://" + getElasticsearchIPAddress() + ":" + getElasticsearchPort());
        log.info("spring.data.elasticsearch.client.reactive.endpoints=http://" + getElasticsearchIPAddress() + ":" + getElasticsearchPort());
        log.info("==============");

    }
}
