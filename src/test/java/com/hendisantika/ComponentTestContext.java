package com.hendisantika;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.context.TestComponent;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-online-supermarket
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/30/22
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */
@TestComponent
@RequiredArgsConstructor
@Data
public class ComponentTestContext {
}
